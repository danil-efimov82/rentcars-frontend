import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { RentalItem } from "src/models";

@Injectable()
export class CarsServiceMock {
  constructor() {
    console.log("CarsServiceMock");
  }

  public find(): Observable<RentalItem[]> {
    return of([]);
  }

  public details(id: number): RentalItem {
    return {
      rentalItemOptions: {
        rentalItemId: id.toString(),
        weeklyDiscount: 10,
        monthDiscount: 20,
        currency: "USD",
      },
      ownerId: 12,
      mark: { name: "Toyota" },
      model: { name: "Camry" },
      year: 2010,
      color: "red",
    };
  }
}
