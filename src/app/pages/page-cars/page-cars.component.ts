import { Component, OnInit } from '@angular/core';
import { CarsService } from 'src/app/services';

@Component({
  selector: 'app-page-cars',
  templateUrl: './page-cars.component.html',
  styleUrls: ['./page-cars.component.scss']
})
export class PageCarsComponent implements OnInit {

  constructor(carsService: CarsService) { }

  ngOnInit(): void {
  }

}
