import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { RentalItem } from 'src/models';

@Injectable()
export class CarsService {

  constructor(private http: HttpClient) { }

  public find(): Observable<RentalItem[]> {
    return of([]);
  }

  public details(id: number): RentalItem {
    return {};
  }
}
