import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageCarsComponent } from './pages/page-cars/page-cars.component';
import { PageCarDetailsComponent } from './pages/page-car-details/page-car-details.component';
import { CarsService } from './services';
import { CarsServiceMock } from './mocks';

@NgModule({
  declarations: [
    AppComponent,
    PageCarsComponent,
    PageCarDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    { provide: CarsService, useClass: environment.mocked ? CarsServiceMock : CarsService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
