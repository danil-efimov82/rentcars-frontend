import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PageCarDetailsComponent } from "./pages/page-car-details/page-car-details.component";
import { PageCarsComponent } from "./pages/page-cars/page-cars.component";

const routes: Routes = [
  { path: "", redirectTo: "/cars", pathMatch: "full" },
  { path: "cars", component: PageCarsComponent },
  { path: "cars/:id/details", component: PageCarDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
