export const environment = {
  production: true,
  mocked: false,
  baseApiUrl: '/api/services',
};
